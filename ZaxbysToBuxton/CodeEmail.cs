﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Data;

namespace ZaxbysToBuxton
{
    class CodeEmail : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public string Bank
        { set; get; }
        public string InFyle
        { set; get; }
        public string OutFyle
        { set; get; }
        public string ExtMsg
        { set; get; }

        /// <summary>Send the email message.</summary>
        /// <returns>True if sent, false if error encountered.</returns>
        public bool SendEmail()
        {
            bool rslt = true;
            string recEmailFyle = string.Empty;
            try
            {

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.Subject = "Buxton Files Ready to Upload";
                message.To.Add(Properties.Settings.Default.emailto.ToString());
                
                StringBuilder sbMessage = new StringBuilder();
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("email.zaxbys.com");
                StringBuilder sbRecEmail = new StringBuilder();
                
                //using (CodeSqlCe clsSqlCe = new CodeSqlCe())
                //{
                //    DataTable dtEmails = clsSqlCe.GetDataset("select [email] from [emaillist]");
                //    foreach (DataRow drEmail in dtEmails.Rows)
                //    {
                //        message.To.Add(drEmail["email"].ToString().Trim());
                //    }
                //}
                message.From = new System.Net.Mail.MailAddress("ablashaw@zaxbys.com", "BUXTON JOB");
                //sbMessage.Append("-----Rerun of 2/8/2017 file with 699 corrections-----" + Environment.NewLine);
                //sbMessage.Append(Environment.NewLine);
                //sbMessage.Append(Environment.NewLine);
                sbMessage.Append("The Monthly Buxton job has run and the files are ready for upload.");
                sbMessage.Append(ExtMsg);
                message.Priority = MailPriority.High;
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.Body = sbMessage.ToString();
                smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = false;
                smtp.Send(message);

                smtp.Dispose();
                message.Dispose();
            }
            catch (Exception ex)
            {
                rslt = false;
            }
            return rslt;
        }
    }
    }
