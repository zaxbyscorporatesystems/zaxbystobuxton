﻿namespace ZaxbysToBuxton
{
   partial class FrmMain
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.pgsbrYear = new System.Windows.Forms.ProgressBar();
            this.pgsbarMonth = new System.Windows.Forms.ProgressBar();
            this.pgsbarStore = new System.Windows.Forms.ProgressBar();
            this.lblYear = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.lblStore = new System.Windows.Forms.Label();
            this.cmdSales = new System.Windows.Forms.Button();
            this.cmdTransactions = new System.Windows.Forms.Button();
            this.lblTransCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxDates = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxYear = new System.Windows.Forms.TextBox();
            this.tbxMonth = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // pgsbrYear
            // 
            this.pgsbrYear.Location = new System.Drawing.Point(30, 69);
            this.pgsbrYear.Name = "pgsbrYear";
            this.pgsbrYear.Size = new System.Drawing.Size(563, 23);
            this.pgsbrYear.TabIndex = 0;
            // 
            // pgsbarMonth
            // 
            this.pgsbarMonth.Location = new System.Drawing.Point(30, 111);
            this.pgsbarMonth.Name = "pgsbarMonth";
            this.pgsbarMonth.Size = new System.Drawing.Size(563, 23);
            this.pgsbarMonth.TabIndex = 1;
            // 
            // pgsbarStore
            // 
            this.pgsbarStore.Location = new System.Drawing.Point(30, 153);
            this.pgsbarStore.Name = "pgsbarStore";
            this.pgsbarStore.Size = new System.Drawing.Size(563, 23);
            this.pgsbarStore.TabIndex = 2;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.ForeColor = System.Drawing.Color.Navy;
            this.lblYear.Location = new System.Drawing.Point(247, 50);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(29, 13);
            this.lblYear.TabIndex = 3;
            this.lblYear.Text = "Year";
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.ForeColor = System.Drawing.Color.Navy;
            this.lblMonth.Location = new System.Drawing.Point(247, 95);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(37, 13);
            this.lblMonth.TabIndex = 4;
            this.lblMonth.Text = "Month";
            // 
            // lblStore
            // 
            this.lblStore.AutoSize = true;
            this.lblStore.ForeColor = System.Drawing.Color.Navy;
            this.lblStore.Location = new System.Drawing.Point(247, 137);
            this.lblStore.Name = "lblStore";
            this.lblStore.Size = new System.Drawing.Size(32, 13);
            this.lblStore.TabIndex = 5;
            this.lblStore.Text = "Store";
            // 
            // cmdSales
            // 
            this.cmdSales.Location = new System.Drawing.Point(75, 13);
            this.cmdSales.Name = "cmdSales";
            this.cmdSales.Size = new System.Drawing.Size(91, 23);
            this.cmdSales.TabIndex = 6;
            this.cmdSales.Text = "Sales";
            this.cmdSales.UseVisualStyleBackColor = true;
            this.cmdSales.Click += new System.EventHandler(this.cmdSales_Click);
            // 
            // cmdTransactions
            // 
            this.cmdTransactions.Enabled = false;
            this.cmdTransactions.Location = new System.Drawing.Point(79, 202);
            this.cmdTransactions.Name = "cmdTransactions";
            this.cmdTransactions.Size = new System.Drawing.Size(91, 23);
            this.cmdTransactions.TabIndex = 7;
            this.cmdTransactions.Text = "Transactions";
            this.cmdTransactions.UseVisualStyleBackColor = true;
            this.cmdTransactions.Click += new System.EventHandler(this.cmdTransactions_Click);
            // 
            // lblTransCount
            // 
            this.lblTransCount.AutoSize = true;
            this.lblTransCount.Enabled = false;
            this.lblTransCount.ForeColor = System.Drawing.Color.Navy;
            this.lblTransCount.Location = new System.Drawing.Point(228, 207);
            this.lblTransCount.Name = "lblTransCount";
            this.lblTransCount.Size = new System.Drawing.Size(66, 13);
            this.lblTransCount.TabIndex = 8;
            this.lblTransCount.Text = "Transaction:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(137, 179);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(264, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "2013 Year only for Transactions";
            // 
            // tbxDates
            // 
            this.tbxDates.Location = new System.Drawing.Point(43, 272);
            this.tbxDates.Multiline = true;
            this.tbxDates.Name = "tbxDates";
            this.tbxDates.Size = new System.Drawing.Size(550, 400);
            this.tbxDates.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(238, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Month:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(373, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Year:";
            // 
            // tbxYear
            // 
            this.tbxYear.Location = new System.Drawing.Point(411, 14);
            this.tbxYear.MaxLength = 4;
            this.tbxYear.Name = "tbxYear";
            this.tbxYear.Size = new System.Drawing.Size(40, 20);
            this.tbxYear.TabIndex = 13;
            // 
            // tbxMonth
            // 
            this.tbxMonth.Location = new System.Drawing.Point(284, 14);
            this.tbxMonth.Name = "tbxMonth";
            this.tbxMonth.Size = new System.Drawing.Size(23, 20);
            this.tbxMonth.TabIndex = 14;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 699);
            this.Controls.Add(this.tbxMonth);
            this.Controls.Add(this.tbxYear);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxDates);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTransCount);
            this.Controls.Add(this.cmdTransactions);
            this.Controls.Add(this.cmdSales);
            this.Controls.Add(this.lblStore);
            this.Controls.Add(this.lblMonth);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.pgsbarStore);
            this.Controls.Add(this.pgsbarMonth);
            this.Controls.Add(this.pgsbrYear);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Buxton";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.ProgressBar pgsbrYear;
      private System.Windows.Forms.ProgressBar pgsbarMonth;
      private System.Windows.Forms.ProgressBar pgsbarStore;
      private System.Windows.Forms.Label lblYear;
      private System.Windows.Forms.Label lblMonth;
      private System.Windows.Forms.Label lblStore;
      private System.Windows.Forms.Button cmdSales;
      private System.Windows.Forms.Button cmdTransactions;
      private System.Windows.Forms.Label lblTransCount;
      private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxDates;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbxYear;
        private System.Windows.Forms.TextBox tbxMonth;
    }
}

