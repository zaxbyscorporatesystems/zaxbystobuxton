﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace ZaxbysToBuxton
{
   public partial class FrmMain : Form
   {
      string storeFyle = string.Empty,
         salesFyle=string.Empty,
         custFyle = string.Empty,
         transFyle = string.Empty,
         totalsFyle = string.Empty,
         extractDate=string.Empty,
         separator="|";
      UInt64 cntStore = 0,
         cntCust = 0,
         cntTrans = 0;
      DateTime strtDate,
         endDate;

      public FrmMain()
      {
         InitializeComponent();
      }

      private void Form1_Load(object sender, EventArgs e)
      {
         extractDate = string.Format("{0:00}/{1:00}/{2}", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year);
            if (DateTime.Now.Month == 1)
            {
                tbxMonth.Text = "12";
                tbxYear.Text = (DateTime.Now.Year - 1).ToString();
            }
            else
            {
                tbxMonth.Text = (DateTime.Now.Month - 1).ToString();
                tbxYear.Text = DateTime.Now.Year.ToString();
            }


            string autorun = Properties.Settings.Default.autoRun.ToString().ToLower();
            if (autorun=="yes")
            {
                doSales();
                Application.Exit();
            }
        }

      private void cmdTransactions_Click(object sender, EventArgs e)
      {
         Cursor = Cursors.WaitCursor;
         getTransactions();
         Cursor = Cursors.Default;
      }


        private void doSales()
        {
            // store file
            int periodInterval = 3,      //3=month     4=quarter
                                   yrToUse = int.TryParse(tbxYear.Text, out yrToUse) ? yrToUse : 0,
                                   mthToUse = int.TryParse(tbxMonth.Text, out mthToUse) ? mthToUse : 0;
            try
            {
                DataSet dsStores = new DataSet(),
                   dsDates = new DataSet();
                StringBuilder sbSql = new StringBuilder(),
                   sbLyne = new StringBuilder();
                storeFyle = string.Format("C://data//Buxton//ZAX_Stores_{0}{1:00}{2:00}.txt", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                if (File.Exists(storeFyle))
                {
                    File.Delete(storeFyle);
                }
                Cursor = Cursors.WaitCursor;
                // get open,pending,closed stores
                sbSql.Append("SELECT [Store_ID]");
                sbSql.Append(",[B50_Name]");
                sbSql.Append(",[Address_1]");
                sbSql.Append(",[Address_2]");
                sbSql.Append(",[City]");
                sbSql.Append(",[State]");
                sbSql.Append(",[Zip_Code]");
                sbSql.Append(",[Open_Status]");
                sbSql.Append(",[Open_Date]");
                sbSql.Append(",[Close_Date]");
                sbSql.Append(",[Licensee_Code]");
                sbSql.Append("FROM [Matrix_All_Stores_vw]");
                sbSql.Append("where [Open_Status] in('Open','Pending','Closed','Transferred')");
                sbSql.Append("order by [Store_ID]");
                string strIDComp = "";
                string strIDList = "";
                string strDupsList = "";
                using (CodeSQL clsSql = new CodeSQL())
                {
                    dsStores = clsSql.CmdDataset(Properties.Settings.Default.aConProd, sbSql.ToString());
                    if (dsStores.Tables.Count > 0 && dsStores.Tables[0].Rows.Count > 0)
                    {
                        cntStore = 0;
                        foreach (DataRow dRow in dsStores.Tables[0].Rows)
                        {
                            strIDComp = "/"+ dRow["Store_ID"].ToString().Trim() + "/";
                            if (strIDList.IndexOf(strIDComp) == -1) //eliminate duplicate store ID's
                            {


                                sbLyne.Clear();
                                if (cntStore == 0)
                                {
                                    //add header AMB 11/17/2017
                                    sbLyne.Append("Date");
                                    sbLyne.Append(separator);
                                    sbLyne.Append("Store_ID");
                                    sbLyne.Append(separator);
                                    sbLyne.Append("B50_Name");
                                    sbLyne.Append(separator);
                                    sbLyne.Append("Address_1");
                                    sbLyne.Append(separator);
                                    sbLyne.Append("Address_2");
                                    sbLyne.Append(separator);
                                    sbLyne.Append("City");
                                    sbLyne.Append(separator);
                                    sbLyne.Append("State");
                                    sbLyne.Append(separator);
                                    sbLyne.Append("Zip_Code");
                                    sbLyne.Append(separator);
                                    sbLyne.Append("Open_Status");
                                    sbLyne.Append(separator);
                                    sbLyne.Append("Open_Date");
                                    sbLyne.Append(separator);
                                    sbLyne.Append("Close_Date");
                                    sbLyne.Append(separator);
                                    sbLyne.Append("Licensee_Code");
                                    sbLyne.Append("\n");
                                }
                                sbLyne.Append(extractDate);
                                sbLyne.Append(separator);
                                sbLyne.Append(dRow["Store_ID"].ToString().Trim());
                                sbLyne.Append(separator);
                                sbLyne.Append(dRow["B50_Name"].ToString().Trim());
                                sbLyne.Append(separator);
                                sbLyne.Append(dRow["Address_1"].ToString().Trim());
                                sbLyne.Append(separator);
                                sbLyne.Append(dRow["Address_2"].ToString().Trim());
                                sbLyne.Append(separator);
                                sbLyne.Append(dRow["City"].ToString().Trim());
                                sbLyne.Append(separator);
                                sbLyne.Append(dRow["State"].ToString().Trim());
                                sbLyne.Append(separator);
                                sbLyne.Append(dRow["Zip_Code"].ToString().Trim());
                                sbLyne.Append(separator);
                                sbLyne.Append(dRow["Open_Status"].ToString().Trim());
                                sbLyne.Append(separator);
                                sbLyne.Append(fixDate(dRow["Open_Date"]));
                                sbLyne.Append(separator);
                                sbLyne.Append(fixDate(dRow["Close_Date"]));
                                sbLyne.Append(separator);
                                sbLyne.Append(dRow["Licensee_Code"].ToString().Trim().ToUpper() == "ZAX" ? "1" : "0");
                                sbLyne.Append("\n");
                                cntStore++;
                                File.AppendAllText(storeFyle, sbLyne.ToString());
                                strIDList = strIDList + "/" + dRow["Store_ID"].ToString().Trim() + "/";
                            }
                            else
                            {
                                strDupsList = strDupsList + strIDComp; //for debugging  should list duplicate store numbers
                            }
                        }
                        // update totals file
                        updateTotalsFyle(storeFyle, cntStore, true);


                        // sales for month ZAX_Sales_yyyymmdd
                        salesFyle = string.Format("C://data//Buxton//ZAX_Sales_{0}{1:00}{2:00}.txt", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        if (File.Exists(salesFyle))
                        {
                            File.Delete(salesFyle);
                        }
                        sbSql.Clear();
                        sbSql.Append("SELECT [Store_ID],[B50_Loc_PK] FROM [Matrix_All_Stores_vw] where [Open_Status] ='Open'  order by [Store_ID]");
                        dsStores = clsSql.CmdDataset(Properties.Settings.Default.aConProd, sbSql.ToString());
                        int strtYr = yrToUse,
                           endYr = yrToUse + 1;
                        pgsbrYear.Maximum = endYr;
                        pgsbrYear.Minimum = strtYr;
                        pgsbarMonth.Minimum = 0;
                        pgsbarMonth.Maximum = 13;
                        pgsbarStore.Minimum = 1;
                        this.Show();
                        pgsbarStore.Maximum = dsStores.Tables[0].Rows.Count + 1;
                        if (dsStores.Tables.Count > 0 && dsStores.Tables[0].Rows.Count > 0)
                        {
                            string loc_PK = string.Empty;
                            foreach (DataRow sDRow in dsStores.Tables[0].Rows)
                            {
                                if (sDRow["B50_Loc_PK"] != null && sDRow["B50_Loc_PK"].ToString().Trim().Length > 0)
                                {
                                    loc_PK += string.Format("{0},", sDRow["B50_Loc_PK"]);
                                }
                            }
                            loc_PK = loc_PK.Substring(0, loc_PK.Length - 1);
                            pgsbrYear.Value = strtYr;
                            pgsbrYear.Update();
                            cntStore = 0;
                            tbxDates.Text = string.Empty;
                            string periodDte = string.Empty;
                            for (int yr = strtYr; yr <= strtYr; yr++)
                            {
                                lblYear.Text = string.Format("Year: {0}", yr);
                                pgsbarMonth.Value = 1;
                                pgsbarMonth.Update();
                                for (int mth = mthToUse; mth <= mthToUse; mth++)
                                {
                                    //if (yr == DateTime.Now.Year
                                    //   && mth > DateTime.Now.Month)
                                    //{
                                    //   break;
                                    //}
                                    lblMonth.Text = string.Format("Month: {0}", mth);
                                    sbSql.Clear();
                                    sbSql.Append("select min([CalDt]) as beginDte, max([CalDt]) as endDte FROM [B50Hosted_CalDay] where [CalID]='9DA74FF3-74A5-46CB-A4EF-EE5F1409B0D8'");
                                    sbSql.Append(string.Format(" and [Yr]={0}", yr));
                                    //sbSql.Append(string.Format(" and [QtrNbr]={0}", mth));             // by quarter
                                    sbSql.Append(string.Format(" and [PerOfYrNbr]={0}", mth));        //?by year
                                                                                                      //sbSql.Append(string.Format(" and [Yr]={0}", DateTime.Now.Year));
                                                                                                      //sbSql.Append(string.Format(" and [PerOfYrNbr]={0}", oMthOfYear));
                                    dsDates = clsSql.CmdDataset(Properties.Settings.Default.aConC3P0, sbSql.ToString());
                                    periodDte = $@"Start: {Convert.ToDateTime(dsDates.Tables[0].Rows[0]["beginDte"]).ToShortDateString()}  End: {Convert.ToDateTime(dsDates.Tables[0].Rows[0]["endDte"]).ToShortDateString()}";
                                    tbxDates.Text = tbxDates.Text + periodDte + Environment.NewLine;
                                    tbxDates.Update();
                                    ////////////////////////////////////////////////////

                                    sbSql.Clear();
                                    sbSql.Append("SELECT [AdjNetSales],[zfi_loc_pk]");
                                    sbSql.Append(" FROM [B50Hosted_Fct_LocRollup]");
                                    sbSql.Append(string.Format(" where [BusDT] = '{0}' and [zfi_loc_pk] in ({1})", dsDates.Tables[0].Rows[0]["endDte"], loc_PK));
                                    //                                    sbSql.Append(" and [Cal_PK]=21 and [XTD] =4");          //4=quarter
                                    sbSql.Append($@" and [Cal_PK]=21 and [XTD] ={periodInterval}");        //3=month
                                    DataSet dsAllSales = clsSql.CmdDataset(Properties.Settings.Default.aConC3P0, sbSql.ToString());

                                    object oSales = null;
                                    decimal dSales = 0;
                                    pgsbarStore.Value = 1;
                                    strIDComp = "";
                                    strIDList = "";
                                    strDupsList = "";
                                    //add header AMB 11-17-2017
                                    File.AppendAllText(salesFyle, "Extract_Date|Store_ID|begin_Dte||DSales\n");
                                    foreach (DataRow dRow in dsStores.Tables[0].Rows)
                                    {
                                        strIDComp = "/" + dRow["B50_Loc_PK"].ToString().Trim() + "/";
                                        if (strIDList.IndexOf(strIDComp) == -1) //avoid store ID dups
                                        {

                                            lblStore.Text = string.Format("Store: {0}", dRow["Store_ID"]);
                                            if (dRow["B50_Loc_PK"].ToString().Trim().Length != 0)
                                            {
                                                DataRow[] drSglSale = dsAllSales.Tables[0].Select(string.Format("[zfi_loc_pk]={0}", dRow["B50_Loc_PK"]));
                                                //sbSql.Clear();
                                                //sbSql.Append("SELECT [AdjNetSales]");
                                                //sbSql.Append(" FROM [B50Hosted_Fct_LocRollup]");
                                                //sbSql.Append(string.Format(" where [BusDT] = '{0}' and [zfi_loc_pk]={1}", dsDates.Tables[0].Rows[0]["endDte"], dRow["B50_Loc_PK"]));
                                                //sbSql.Append(" and [Cal_PK]=21 and [XTD] =3");
                                                try
                                                {
                                                    oSales = drSglSale[0]["AdjNetSales"];     // clsSql.CmdScalar(Properties.Settings.Default.aConC3P0, sbSql.ToString());
                                                    if (oSales != null)
                                                    {
                                                        dSales = decimal.TryParse(oSales.ToString(), out dSales) ? dSales : 0;
                                                        sbLyne.Clear();
                                                        sbLyne.Append(extractDate);
                                                        sbLyne.Append(separator);
                                                        sbLyne.Append(dRow["Store_ID"].ToString().Trim());
                                                        sbLyne.Append(separator);
                                                        // first day of month
                                                        sbLyne.Append(Convert.ToDateTime(dsDates.Tables[0].Rows[0]["beginDte"]).ToShortDateString());
                                                        sbLyne.Append(separator);
                                                        sbLyne.Append(separator);
                                                        sbLyne.Append(string.Format("{0:N2}", dSales));
                                                        sbLyne.Append("\n");
                                                        File.AppendAllText(salesFyle, sbLyne.ToString());
                                                        cntStore++;
                                                    }
                                                }
                                                catch
                                                {
                                                }
                                            }
                                            pgsbarStore.Value += 1;
                                            this.Update();
                                            strIDList = strIDList + strIDComp;
                                        }
                                        else
                                        {
                                            strDupsList = strDupsList + strIDComp;
                                        }
                                    }
                                    pgsbarMonth.Value += 1;
                                    this.Update();
                                }
                                pgsbrYear.Value += 1;
                                this.Update();
                            }
                            // update totals file
                            updateTotalsFyle(salesFyle, cntStore, false);
                        }
                    }
                }
                //using (WebClient client = new WebClient())
                //{
                //   client.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                //   client.UploadFile("ftp://ftpserver.com/target.zip", "STOR", localFilePath);
                //}


                CodeEmail myemail = new CodeEmail();
                myemail.SendEmail();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            Cursor = Cursors.Default;
        }


      private void cmdSales_Click(object sender, EventArgs e)
      {
            doSales();
      }

      private void getTransactions()
      {
         StringBuilder sbSql = new StringBuilder(),
            sbLyne=new StringBuilder();
         DataSet dsStores = new DataSet(),
            dsTrans = new DataSet();
         //int b50Loc = 0;
         Cursor = Cursors.WaitCursor;
         try
         {
            // transactions ZAX_Transactions_yyyymmdd
            using (CodeSQL clsSql = new CodeSQL())
            {
               sbSql.Clear();
               //sbSql.Append("SELECT [Store_ID],[B50_Loc_PK] FROM [Matrix_All_Stores_vw] where [Open_Status] ='Open' order by [Store_ID]");
              // sbSql.Append("SELECT [st_id],[st_loc_code] FROM [Matrix_Store] where [st_open_status]='O'");

              // dsStores = clsSql.CmdDataset(Properties.Settings.Default.aConProd, sbSql.ToString());
               strtDate = new DateTime(2015, 7, 27);
               endDate = new DateTime(2015, 11, 22);
               DateTime dteToUse = new DateTime();
               transFyle = string.Format("C://data//Buxton//ZAX_Transactions_{0}{1:00}{2:00}_to_{3}{4:00}{5:00}.txt", strtDate.Year, strtDate.Month, strtDate.Day, endDate.Year, endDate.Month, endDate.Day);
               if (File.Exists(transFyle))
               {
                  File.Delete(transFyle);
               }

               int strtYr = 2015,
                  endYr = 2015,
                  locpk;
               pgsbrYear.Maximum = endYr;
               pgsbrYear.Minimum = strtYr;
               pgsbarMonth.Minimum = 0;
               pgsbarMonth.Maximum = 13;
               pgsbarStore.Minimum = 1;
               this.Show();
               cntTrans = 0;
               dteToUse = strtDate;
               
               while (dteToUse <= endDate)
               {
                  sbSql.Clear();
                  //sbSql.Append("SELECT [TicketNbr],[TotalAmt],[OrdEntryTime],[zfi_loc_pk]  FROM [B50Hosted_Fct_Trans]");
                  //sbSql.Append(string.Format("where CONVERT(VARCHAR(10), [OrdEntryTime],111)>='{0}/{1:00}/{2:00}'", strtDate.Year, strtDate.Month, strtDate.Day));
                  //sbSql.Append(string.Format("and CONVERT(VARCHAR(10), [OrdEntryTime],111)<='{0}/{1:00}/{2:00}'", endDate.Year, endDate.Month, endDate.Day));
                  sbSql.Append("SELECT [TicketNbr],[TotalAmt],[OrdEntryTime],[zfi_loc_pk]  FROM [B50Hosted_Fct_Trans]");
                  sbSql.Append(string.Format("where CONVERT(VARCHAR(10), [OrdEntryTime],111)='{0}/{1:00}/{2:00}'", dteToUse.Year, dteToUse.Month, dteToUse.Day));
                  dsTrans = clsSql.CmdDataset(Properties.Settings.Default.aConC3P0, sbSql.ToString());
                  //sbSql.Append("SELECT [TicketNbr],[TotalAmt],[OrdEntryTime],[zfi_loc_pk]  FROM [fct_trans]");
                  //sbSql.Append(string.Format("where CONVERT(VARCHAR(10), [OrdEntryTime],111)='{0}/{1:00}/{2:00}'", dteToUse.Year, dteToUse.Month, dteToUse.Day));
                  //dsTrans = clsSql.CmdDataset(Properties.Settings.Default.aConDak, sbSql.ToString());
                  DataRow[] drStoreID;
                  object storID=null;
                  if (dsTrans.Tables.Count > 0 && dsTrans.Tables[0].Rows.Count > 0)
                  {
                     dsStores = clsSql.CmdDataset(Properties.Settings.Default.aConProd, "SELECT [st_loc_code],[st_id] FROM [Matrix_Store]where [st_loc_code] is not null and [st_loc_code]<>''");
                     foreach (DataRow dRow in dsTrans.Tables[0].Rows)
                     {
                        try
                        {
                           locpk = Convert.ToInt32(dRow["zfi_loc_pk"].ToString().Trim());
                           drStoreID = dsStores.Tables[0].Select(string.Format("[st_loc_code]='{0}'", locpk));
                           storID = drStoreID[0]["st_id"];
                           sbLyne.Clear();
                           sbLyne.Append(extractDate);
                           sbLyne.Append(separator);
                           sbLyne.Append(dRow["TicketNbr"]);
                           sbLyne.Append(separator);
                           // store number from b50
                           ////                     string look = string.Format("B50_Loc_PK={0}", locpk);
                           //                     string look = "st_loc_code=" + locpk.ToString();
                           //sbSql.Clear();
                           //sbSql.Append(string.Format("SELECT [st_id] FROM [Matrix_Store] where [st_loc_code]={0}", locpk));
                           //DataRow[] drStoreID = dsStores.Tables[0].Select(look);     //4  "zfi_loc_pk"
                           sbLyne.Append(storID);
                           //sbLyne.Append(clsSql.CmdScalar(Properties.Settings.Default.aConProd, sbSql.ToString()));
                           sbLyne.Append(separator);
                           //trans datatime
                           sbLyne.Append(dRow["OrdEntryTime"]);
                           sbLyne.Append(separator);
                           //dept not used
                           sbLyne.Append(separator);
                           //trans amount
                           sbLyne.Append(string.Format("{0:N2}", dRow["TotalAmt"]));
                           sbLyne.Append("\n");
                           File.AppendAllText(transFyle, sbLyne.ToString());
                           cntTrans++;
                           lblTransCount.Update();
                           this.Update();
                        }
                        catch (Exception ex)
                        {
                           //MessageBox.Show(string.Format("{0} [st_loc_code] drStoreID ", ex.Message ), "Error");
                        }
                     }
                  }
                  lblTransCount.Text = string.Format("Transactions from: {0} saved.", dteToUse.ToShortDateString());
                  lblTransCount.Update();
                  dteToUse=dteToUse.AddDays(1);
               }
               // end
               updateTotalsFyle(transFyle, cntTrans, false);

            
            }
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message, "Error");
         }
         Cursor = Cursors.Default;
      }

      string corpStore(object licCode)
      {
         string rslt = licCode.ToString().Trim().ToUpper()=="ZAX"?"1":"0";
         return rslt;
      }
      string fixDate(object oDte)
      {
         string rslt = string.Empty;
         if (oDte != null&& oDte.ToString().Trim().Length>0)
         {
            string[] dats = Convert.ToDateTime(oDte).ToShortDateString().Split('/');
            if (Convert.ToInt16(dats[2]) < 50)
            {
               rslt = string.Format("{0:00}/{1:00}/{2:00}", dats[0], dats[1], dats[2]);
            }
            else
            {
               rslt = string.Format("{0:00}/{1:00}/{2:00}", dats[0], dats[1], dats[2]);
            }
         }
         return rslt;
      }
      void updateTotalsFyle(string curFyle, UInt64 cntRec,bool first)
      {
        // totalsFyle = string.Format("C://data//Buxton//ZAX_2015_Transactions_only_Totals_{0}{1:00}{2:00}.txt", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
         totalsFyle = string.Format("C://data//Buxton//ZAX_Transactions_only_Totals_{0}{1:00}{2:00}.txt", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
         if (File.Exists(totalsFyle))
         {
            string[] lynes = File.ReadAllLines(totalsFyle);
            File.Delete(totalsFyle);
            if (!first)
            {
               foreach (string lyne in lynes)
               {
                  if (!lyne.Contains(curFyle))
                  {
                     File.AppendAllText(totalsFyle, lyne + "\n");
                  }
               }
            }
         }
         File.AppendAllText(totalsFyle, extractDate + "|" + curFyle.Substring(curFyle.LastIndexOf("//")+2) + "|" + cntRec.ToString() + "\n");


      }

   }
}
